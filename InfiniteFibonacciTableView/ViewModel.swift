//
//  ViewModel.swift
//  InfiniteFibonacciTableView
//
//  Created by Sergio Andres Rodriguez Castillo on 02/01/24.
//

import Foundation

class ViewModel {
    var fibonacciLimit = 0
    var fibonacciStartNumber = 0
    var fibonacciSeries: [Int64] = []
    var fibonacciCache = [Int: Int64]()
    
    func fillFibonacciSeries() {
        fibonacciLimit = fibonacciLimit + 14
        for value in fibonacciStartNumber...fibonacciLimit {
            fibonacciSeries.append(calculateFibonacciSeries(num: value))
        }
        fibonacciStartNumber = fibonacciSeries.count
    }
    
    func calculateFibonacciSeries(num: Int) -> Int64 {
        if let result = fibonacciCache[num] {
            return result
        }
        
        var result: Int64
        if num <= 1 {
            result = Int64(num)
        } else {
            result = calculateFibonacciSeries(num: num - 1) + calculateFibonacciSeries(num: num - 2)
        }
        
        fibonacciCache[num] = result
        return result
    }
    
    func getNumberOfRows() -> Int {
        return fibonacciSeries.count + 1
    }
    
    func getCurrentDataCount() -> Int {
        return fibonacciSeries.count
    }
    
    func getCurrentNumberForIndexPath(indexPath: IndexPath) -> Int64? {
        return fibonacciSeries[safe: indexPath.row]
    }
}
