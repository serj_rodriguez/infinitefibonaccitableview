//
//  Helpers.swift
//  InfiniteFibonacciTableView
//
//  Created by Sergio Andres Rodriguez Castillo on 02/01/24.
//

import Foundation

extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
