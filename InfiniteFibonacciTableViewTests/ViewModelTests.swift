//
//  ViewModelTests.swift
//  InfiniteFibonacciTableViewTests
//
//  Created by Sergio Andres Rodriguez Castillo on 02/01/24.
//

import XCTest
@testable import InfiniteFibonacciTableView

final class ViewModelTests: XCTestCase {
    var sut: ViewModel!
    
    override func setUp() {
        super.setUp()
        sut = ViewModel()
    }
    
    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_fillFibonacciSeries_shouldFillFirst15FibonacciNumbers() {
        let testInitialArray: [Int64] = [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377]
        sut.fillFibonacciSeries()
        
        XCTAssertEqual(sut.fibonacciSeries.count, 15)
        XCTAssertEqual(sut.fibonacciSeries, testInitialArray)
    }
    
    func test_calculateFibonacciSeriesWithZeroAsInput_shouldReturnZeroAsResult() {
        XCTAssertEqual(sut.calculateFibonacciSeries(num: 0), 0)
    }
    
    func test_calculateFibonacciSeriesWithOneAsInput_shouldReturnOneAsResult() {
        XCTAssertEqual(sut.calculateFibonacciSeries(num: 1), 1)
    }
    
    func test_calculateFibonacciSeriesWithTwoAsInput_shouldReturnThreeAsResult() {
        XCTAssertEqual(sut.calculateFibonacciSeries(num: 2), 3)
    }
    
    func test_getNumberOfRows_shouldReturnFibonacciSeriesCountArrayPlusOne() {
        sut.fibonacciSeries = [0,1,1]
        XCTAssertEqual(sut.fibonacciSeries.count, 3)
        
        let result = sut.getNumberOfRows()
        XCTAssertEqual(result, 4)
    }
    
    func test_getCurrentDataCount_shouldReturnFibonacciSeriesArrayCount() {
        sut.fibonacciSeries = [0,1,1]
        let result = sut.getCurrentDataCount()
        XCTAssertEqual(result, 3)
    }
}
